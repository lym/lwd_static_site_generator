# Static Site Generator

# Usage
Download Dependencies:

    $ env/bin/pip3 install -r requirements.txt

To generate and output our static site

    .env/bin/python prototypes.py build

To test the new static pages generated, cd into `_build` and start
a simple python server

    $ cd _build

    $ .env/bin/python -m http.server 9000

The files in the `_build` directory can be deployed to any static web
page server and they will render as intended.

To compile a single page into static content:

    $ .env/bin/python prototypes.py build index
